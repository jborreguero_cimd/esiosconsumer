package com.w2m.esios.esios_consumer.service;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.json.simple.JSONObject;

public class ProducibleHidraulicoService {

	
	public Map<String, Object> generateInsertMapProducibleHidraulico(JSONObject val) throws ParseException {
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		DateTime fechaConsulta = new DateTime(val.get("fechaConsulta"));
        double valorDiario = Double.parseDouble((String) val.get("valorDiario"));
        double valorMensual = Double.parseDouble((String) val.get("valorMensual"));
      
        result.put("fechaConsulta",new java.sql.Timestamp(fechaConsulta.toDate().getTime()));
        result.put("valorDiario",valorDiario);
        result.put("valorMensual",valorMensual);
		
		return result;
	}
	
}
