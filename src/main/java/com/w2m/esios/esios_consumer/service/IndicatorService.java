package com.w2m.esios.esios_consumer.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.joda.time.DateTime;
import org.json.simple.JSONObject;

import com.w2m.esios.esios_consumer.dto.SqlServerDTO;
import com.w2m.esios.esios_consumer.model.Indicator;

public class IndicatorService {

	private SqlServerDTO dto;
	
	public IndicatorService() {
		this.dto = new SqlServerDTO("ESIOS", "INDICATOR_");
	}

	public void insertFile(String filePath) {

		try {

			Indicator indicator = new Indicator(filePath);
			String codIndicator = indicator.getCodIndicator();
			
			dto.checkTableIndicator(codIndicator);

			// Iterar y cargar datos
			Iterator<JSONObject> valuesIterator = indicator.getValuesIterator();
			
			while (valuesIterator.hasNext()) {

				JSONObject val = valuesIterator.next();

				Map<String, Object> dataMap = this.generateInsertMap(val);

				dto.insertIndicator(dataMap, codIndicator);

			}

		} catch (ParseException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
	}
	

	private Map<String, Object> generateInsertMap(JSONObject val) throws ParseException {
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		DateTime datetime = new DateTime(val.get("datetime"));
        DateTime datetime_utc = new DateTime(val.get("datetime_utc"));
        double value = (double) val.get("value");
    	long geo_id = (long) val.get("geo_id");
        String geo_name = (String) val.get("geo_name");
      
        result.put("datetime",new java.sql.Timestamp(datetime.toDate().getTime()));
        result.put("datetime_utc",new java.sql.Timestamp(datetime_utc.toDate().getTime()));
        result.put("value",value);
        result.put("geo_id",geo_id);
        result.put("geo_name",geo_name);
		
		return result;
	}
	
	
}
