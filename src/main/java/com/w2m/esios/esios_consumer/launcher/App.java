package com.w2m.esios.esios_consumer.launcher;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.JsonElement;
import com.w2m.esios.esios_consumer.dto.SqlServerDTO;

public class App 
{
    public static void main( String[] args ) throws SQLException
    {
    	
    	try {
            Files.walk(Paths.get("C:\\work\\workspaces\\w_python\\EsiosDownloader\\indicators\\20190723")).parallel().forEach(file -> {
            
            	String filePath = file.toAbsolutePath().toString();
            	
            	if(filePath.contains("json")) {
            		try {
            	        JSONParser parser = new JSONParser();
            	        JSONObject data = (JSONObject) parser.parse(
            	              new FileReader(filePath));
            	        
            	        JSONObject indicators = (JSONObject)data.get("indicator");
            	        JSONArray values = (JSONArray)indicators.get("values");

            	        //Obtengo el id del indicador
            	        long cod_indicator = (long) indicators.get("id");
            	        
            	        //Comprobamos la tabla donde se cargarán
            	        SqlServerDTO dto = new SqlServerDTO();
            	        dto.checkTableIndicator(Long.toString(cod_indicator));
//            	        
            	        //Iteramos y cargamos los datos
            	        Iterator<JSONObject> it = values.iterator();
            	        while(it.hasNext()){
	            	        
            	        	JSONObject val = it.next();
            	        	
            	        	Map<String,Object> dataMap = dto.generateInsertMap(val);
	            	        
	            	        dto.insertIndicator(dataMap,Long.toString(cod_indicator));
	            	        
            	        	
//            	            //Test mongo
//                	        MongoConnection mongoCon =  MongoConnection.getInstance();
//                	        MongoCollection col = mongoCon.getCollection("t1");
//                	        
//                	        System.out.println(col.getNamespace());
////                	        collection.insertOne(); 
            	        	
	            	       
	            	       }
            	        
            	        System.out.println("fichero Procesado: "+file.getFileName());
            	        
            	    } catch (IOException | ParseException e) {
            	        e.printStackTrace();
            	    } catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (java.text.ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            	
            
            });
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    
    
}
