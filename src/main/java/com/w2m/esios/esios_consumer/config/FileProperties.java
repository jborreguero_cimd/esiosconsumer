package com.w2m.esios.esios_consumer.config;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class FileProperties {

	private static Properties properties = null;

	public static void singletonFileProperties(String rutaFichero) {
	    if (properties == null) {
	    	properties = new Properties();
			try {
				properties.load(new FileReader(rutaFichero));
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	}
	
	public static Properties getPropertiesFile() {
		return properties;
	}
	
}
