package com.w2m.esios.esios_consumer.config;

import java.util.Properties;

public class ReadProperty {

	
	
	public static String getPropertyString(String prop) {
	
		String resultado;
		
		Properties propertiesFile = FileProperties.getPropertiesFile();
		
		resultado = propertiesFile.getProperty(prop);
		
		return resultado;
	
	}
	
	
	public static int getPropertyInt(String prop) {
		int resultado;
		Properties propertiesFile = FileProperties.getPropertiesFile();
		
		resultado = Integer.parseInt(propertiesFile.getProperty(prop));
		
		return resultado;
	
	}
	
	
}
