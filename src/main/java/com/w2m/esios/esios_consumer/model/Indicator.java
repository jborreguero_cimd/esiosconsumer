package com.w2m.esios.esios_consumer.model;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Indicator {
	
	private JSONObject indicators;
	private JSONArray values;
	
	public Indicator(String filePath) throws FileNotFoundException, IOException, ParseException {
		
		JSONObject data = this.parseFile(filePath);

		this.indicators = (JSONObject) data.get("indicator");
		this.values = (JSONArray) indicators.get("values");

	}
	
	private JSONObject parseFile(String filePath) throws FileNotFoundException, IOException, org.json.simple.parser.ParseException {
		JSONParser parser = new JSONParser();
		JSONObject data = (JSONObject) parser.parse(new FileReader(filePath));

		return data;
	}
	
	
	@SuppressWarnings("unchecked")
	public Iterator<JSONObject> getValuesIterator(){
		return values.iterator();
		
	}
	
	
	public String getCodIndicator() {
		return Long.toString(this.getCodIndicator_Interanl());
	}
	
	private Long getCodIndicator_Interanl() {
		return (long) indicators.get("id");
		
	}
}
