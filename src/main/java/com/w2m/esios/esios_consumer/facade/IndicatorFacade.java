package com.w2m.esios.esios_consumer.facade;

import com.w2m.esios.esios_consumer.controller.IndicatorController;

public class IndicatorFacade {

	private IndicatorController indicatorCtrl;
	
	public IndicatorFacade() {
		this.indicatorCtrl = new IndicatorController();
	}
	
	/**
	 * Inserta todos los indicadores existentes en el directorio
	 */
	public void launch() {
		indicatorCtrl.manageFiles();
	}
}
