package com.w2m.esios.esios_consumer.controller;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.w2m.esios.esios_consumer.dto.SqlServerDTO;

public class ProducibleHidraulicoController {

	 public static void main( String[] args ) {
	    	
		try {
			Files.walk(Paths.get("C:\\work\\workspaces\\w_python\\EsiosDownloader\\producibleHidraulico\\20190724")).parallel()
					.forEach(file -> {

						String filePath = file.toAbsolutePath().toString();

						if (filePath.contains("json")) {
							try {
								JSONParser parser = new JSONParser();
								JSONObject data = (JSONObject) parser.parse(new FileReader(filePath));

								// Comprobamos la tabla donde se cargarán
								SqlServerDTO dto = new SqlServerDTO();
								dto.checkTableProducibleHidraulico("producibleHidraulico");
								
								Map<String, Object> dataMap = dto.generateInsertMapProducibleHidraulico(data);
								
								
								dto.insertProducibleHidraulico(dataMap);


								System.out.println("fichero Procesado: " + file.getFileName());

							} catch (IOException | ParseException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (java.text.ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					});
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
