package com.w2m.esios.esios_consumer.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.w2m.esios.esios_consumer.service.IndicatorService;

public class IndicatorController {
	
	private IndicatorService indicatorService;
	
	public IndicatorController() {
		this.indicatorService = new IndicatorService();
	}

	public void manageFiles() {
		
		String dirPath = this.getDirPath();
		
		try {
            Files.walk(Paths.get(dirPath)).parallel().forEach(file -> {
            	
            	String filePath = file.toAbsolutePath().toString();
            	if(filePath.contains("json")) {
            		indicatorService.insertFile(filePath);
            	}
            	
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		
	}
	
	private String getDirPath() {
		String filePath = "C:\\work\\workspaces\\w_python\\EsiosDownloader\\indicators\\20190725";
		
		return filePath;
		
	}
}