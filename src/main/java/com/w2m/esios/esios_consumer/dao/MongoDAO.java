package com.w2m.esios.esios_consumer.dao;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDAO {

	private static MongoDAO instance;
    private static MongoDatabase database;
   

    private MongoDAO() {
        MongoClient client = new MongoClient("localhost",27017);
        MongoDatabase database = client.getDatabase("test"); 
    }

    public static MongoDAO getInstance() {
        if (instance == null) {
            synchronized (MongoDAO.class) {
                if (instance == null) { 
                    instance = new MongoDAO();
                }
            }
        }

        return instance;
    }
    
    public MongoCollection getCollection(String collectionName) {
        return database.getCollection(collectionName);
    }

}
