package com.w2m.esios.esios_consumer.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.w2m.esios.esios_consumer.config.ReadProperty;

public class SqlServerDAO {

	private static SqlServerDAO instance;
	private Connection connection;
	private String url = ReadProperty.getPropertyString("BBDD_ipport");
	private String username = ReadProperty.getPropertyString("BBDD_username");
	private String password = ReadProperty.getPropertyString("BBDD_password");
	private String driverClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	private SqlServerDAO(String schema) {
		try {
			String connectionUrl = "jdbc:sqlserver://"+url+";databaseName="+schema+";user="+username+";password="+password;
			Class.forName(driverClass);
			
			this.connection = DriverManager.getConnection(connectionUrl);
		} catch (ClassNotFoundException | SQLException ex) {
			System.out.println("Database Connection Creation Failed : " + ex.getMessage());
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public static SqlServerDAO getInstance(String schema) throws SQLException {
		if (instance == null) {
			instance = new SqlServerDAO(schema);
		} else if (instance.getConnection().isClosed()) {
			instance = new SqlServerDAO(schema);
		}

		return instance;
	}
}
