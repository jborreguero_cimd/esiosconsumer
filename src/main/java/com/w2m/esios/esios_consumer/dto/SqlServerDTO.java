package com.w2m.esios.esios_consumer.dto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Map;
import java.util.TimeZone;

import com.w2m.esios.esios_consumer.dao.SqlServerDAO;

public class SqlServerDTO {
	
	private String tableName;
	private String databaseName;
	private SqlServerDAO dc;
	private Connection con;
	
	public SqlServerDTO(String databaseName, String tableName) {
		this.databaseName = databaseName;
		this.tableName = tableName;
		openConnection();
	}
	
	private void openConnection()  {
		try {
			this.dc = SqlServerDAO.getInstance(this.databaseName);
			this.con = dc.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized int insertIndicator(Map<String,Object> dataMap,String indicator) throws SQLException  {
		
        PreparedStatement preparedStatement = con
                .prepareStatement("insert into "+tableName+""+indicator+" values (?, ?, ?, ?, ?)");

        preparedStatement.setDouble(1, (double) dataMap.get("value"));
        preparedStatement.setTimestamp(2, (java.sql.Timestamp) dataMap.get("datetime"));
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        preparedStatement.setTimestamp(3, (java.sql.Timestamp) dataMap.get("datetime_utc"),cal);
        preparedStatement.setLong(4, (long) dataMap.get("geo_id"));
        preparedStatement.setString(5, (String) dataMap.get("geo_name"));

        int status = preparedStatement.executeUpdate();
        
		return status;
		
	}
	
	
	public synchronized int checkTableIndicator(String indicator) throws SQLException {
        
		String sqlCreate = "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='"+ tableName +"" + indicator + "' and xtype='U')"
				+ " CREATE TABLE INDICATOR_" + tableName + "  (value           REAL,"
				+ "   datetime            DATETIME," + "   datetime_utc          DATETIME,"
				+ "   geo_id           REAL," + "   geo_name     VARCHAR(50))";
        
        Statement stmt = con.createStatement();
        stmt.execute(sqlCreate);
		
		return 0;
		
	}
	
		
	public synchronized int checkTableProducibleHidraulico() throws SQLException {
		
        
		String sqlCreate = "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='"+ tableName +"' and xtype='U')"
				+ " CREATE TABLE PRODUCIBLE_HIDRAULICO  (diario           REAL,"
				+ " mensual REAL, "
				+ "   fecha_consulta            DATETIME," + "   fecha_consulta_utc          DATETIME)";
        
        Statement stmt = con.createStatement();
        stmt.execute(sqlCreate);
		
		return 0;
		
	}

	
	public synchronized int insertProducibleHidraulico(Map<String,Object> dataMap) throws SQLException {
		
        PreparedStatement preparedStatement = con
                .prepareStatement("insert into "+tableName+" values (?, ?, ?, ?)");

        preparedStatement.setDouble(1, (double) dataMap.get("valorDiario"));
        preparedStatement.setDouble(2, (double) dataMap.get("valorMensual"));
        preparedStatement.setTimestamp(3, (java.sql.Timestamp) dataMap.get("fechaConsulta"));
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        preparedStatement.setTimestamp(4, (java.sql.Timestamp) dataMap.get("fechaConsulta"),cal);

        int status = preparedStatement.executeUpdate();
        
		return status;
		
	}
}
